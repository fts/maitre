/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"encoding/json"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.cern.ch/flutter/stomp"
	"gitlab.cern.ch/fts/maitre/maitre/messages"
	"os"
)

type (
	stompDestinations struct {
		State    string
		Complete string
	}
)

var (
	stompParams  stomp.ConnectionParameters
	destinations stompDestinations
)

func merge(channels ...<-chan error) <-chan error {
	out := make(chan error)

	for _, c := range channels {
		go func(c <-chan error) {
			for e := range c {
				out <- e
			}
		}(c)
	}

	return out
}

func subscribe(conn *stomp.Consumer, destination, id string) (msgChannel <-chan stomp.Message, errorChannel <-chan error) {
	var err error
	msgChannel, errorChannel, err = conn.Subscribe(destination, id, stomp.AckAuto)
	if err != nil {
		log.Fatal(err)
	}
	log.Info("Subscribed to ", destination)
	return msgChannel, errorChannel
}

func extractBody(msg *stomp.Message) []byte {
	// Suppress last \0x04
	return msg.Body[0 : len(msg.Body)-1]
}

var updateCmd = &cobra.Command{
	Use: "update",
	Run: func(cmd *cobra.Command, args []string) {
		consumer, err := stomp.NewConsumer(stompParams)
		if err != nil {
			log.Fatal(err)
		}
		defer consumer.Close()

		stateChan, stateErrorChan := subscribe(consumer, destinations.State, "maitre-state")
		completeChan, completeErrorChan := subscribe(consumer, destinations.Complete, "maitre-complete")

		mergedErrorChan := merge(stateErrorChan, completeErrorChan)

		for {
			select {
			case msg := <-stateChan:
				data := extractBody(&msg)
				state := &messages.StateChange{}
				if err := json.Unmarshal(data, state); err != nil {
					log.Warn("State: ", err)
					log.Debug(string(data))
				}

				if err := instance.FeedStateChange(state); err != nil {
					log.Warn(err)
				}
			case msg := <-completeChan:
				data := extractBody(&msg)
				end := &messages.Terminal{}
				if err := json.Unmarshal(data, end); err != nil {
					log.Warn("Complete: ", err)
					log.Debug(string(data))
				}
				if err := instance.FeedTransferTerminal(end); err != nil {
					log.Warn(err)
				}
			case err := <-mergedErrorChan:
				log.Warn(err)
			}
		}
	},
}

func init() {
	flags := updateCmd.PersistentFlags()
	flags.StringVar(&stompParams.Address, "stomp-address", "dashb-mb.cern.ch:61123", "Stomp host:port")
	flags.StringVar(&stompParams.Login, "stomp-login", "", "Stomp login")
	flags.StringVar(&stompParams.Passcode, "stomp-passcode", "", "Stomp passcode")
	flags.BoolVar(&stompParams.EnableTLS, "stomp-tls", true, "Enable or disable tls (on by default)")
	// Sadly, given that we have to subscribe to each one of the hosts behind the dns entry, need to be insecure
	flags.BoolVar(&stompParams.Insecure, "stomp-insecure", true, "Do not verity the certificate")
	flags.StringVar(&stompParams.CaPath, "stomp-capath", "/etc/grid-security/certificates", "CA Path")
	flags.StringVar(&stompParams.CaCert, "stomp-cacert", "", "CA Cert")
	flags.StringVar(&stompParams.UserCert, "cert", os.Getenv("X509_USER_CERT"), "User certificate")
	flags.StringVar(&stompParams.UserKey, "key", os.Getenv("X509_USER_KEY"), "User private key")

	flags.StringVar(&destinations.State, "destination-state", "/topic/transfer.fts_monitoring_state",
		"Subscribe to this for state changes")
	flags.StringVar(&destinations.Complete, "destination-complete", "/topic/transfer.fts_monitoring_complete",
		"Subscribe to this for completion messages")

	stompParams.ClientID = "maitre"
	stompParams.ConnectionLost = func(c *stomp.Broker) {
		log.Warn("Reconnecting to ", c.RemoteAddr())
		if err := c.Reconnect(); err != nil {
			log.Fatal(err)
		}
	}

	rootCmd.AddCommand(updateCmd)
}
