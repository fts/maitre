/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.cern.ch/fts/maitre/maitre"
	"os"
)

var (
	debug       bool
	redisParams maitre.Parameters

	instance *maitre.Maitre
)

var rootCmd = &cobra.Command{
	Use: "maitre",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		log.SetOutput(os.Stderr)
		if debug {
			log.SetLevel(log.DebugLevel)
		}
		var err error
		instance, err = maitre.New(redisParams)
		if err != nil {
			log.Fatal(err)
		}
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		instance.Close()
	},
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Usage()
	},
}

func init() {
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "Enable debug logging")
	rootCmd.PersistentFlags().StringVar(&redisParams.Address, "redis-address", "localhost:6379", "Redis address")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
