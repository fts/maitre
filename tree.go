/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/ayllon/go/algo"
	"github.com/spf13/cobra"
	"github.com/tmc/dot"
	"gitlab.cern.ch/fts/maitre/maitre"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
)

var (
	sources    []string
	vo         string
	filesize   int64
	dotfile    string
	predotpath string
	autoview   bool
)

func in(str string, where []string) bool {
	for i := range where {
		if where[i] == str {
			return true
		}
	}
	return false
}

func newNode(graph *dot.Graph, nodes map[string]*dot.Node, name string, sources []string) *dot.Node {
	if node, ok := nodes[name]; ok {
		return node
	}
	node := dot.NewNode(name)
	nodes[name] = node

	if name == "" {
		// Root
		node.Set("fillcolor", "#778899")
		node.Set("style", "filled")
	} else if in(name, sources) {
		// Source storage
		node.Set("fillcolor", "#90EE90")
		node.Set("style", "filled")
	}

	graph.AddNode(node)
	return node
}

func generateDotFile(sources []string, edges []algo.Edge, out io.Writer) error {
	nodes := make(map[string]*dot.Node)
	graph := dot.NewGraph("span")
	for _, e := range edges {
		link, ok := e.(maitre.TransferEdge)
		var label string
		if ok {
			label = link.EstimatedTime.String()
		} else {
			label = "???"
		}

		src := newNode(graph, nodes, e.GetSource(), sources)
		dst := newNode(graph, nodes, e.GetDestination(), sources)

		edge := dot.NewEdge(src, dst)
		edge.Set("label", label)
		graph.AddEdge(edge)
	}
	_, err := out.Write([]byte(graph.String()))
	return err
}

func generateAndViewPng(dotpath string) {
	pngpath := dotpath + ".png"
	cmd := exec.Command("dot", dotpath, "-Tpng", "-o", pngpath)
	if err := cmd.Run(); err != nil {
		log.Warn(err)
	} else {
		view := exec.Command("xdg-open", pngpath)
		view.Run()
	}
}

var treeCmd = &cobra.Command{
	Use: "tree",
	Run: func(cmd *cobra.Command, args []string) {
		if len(sources) == 0 {
			log.Fatal("At least one source is needed")
		}
		if len(args) == 0 {
			log.Fatal("At least one destination is needed")
		}

		task := &maitre.TaskDescription{
			FileSize:     filesize,
			VoName:       vo,
			Sources:      sources,
			Destinations: args,
		}

		var err error
		var predotFd, postdotFd *os.File

		if predotpath != "" {
			predotFd, err = os.Create(predotpath)
		} else if autoview {
			predotFd, err = ioutil.TempFile("", "maitre-pre")
		}

		if err != nil {
			log.Fatal(err)
		}

		if predotFd != nil {
			defer predotFd.Close()
			if edges, err := instance.GenerateEdgeList(task); err != nil {
				log.Error(err)
			} else if err := generateDotFile(sources, edges, predotFd); err != nil {
				log.Error(err)
			}
			predotFd.Sync()
		}

		edges, err := instance.SpanningTree(task)
		if err != nil {
			log.Fatal(err)
		}
		for i, edge := range edges {
			fmt.Println(i, " ", edge.GetSource(), " => ", edge.GetDestination())
		}

		if dotfile != "" {
			postdotFd, err = os.Create(dotfile)
			if err != nil {
				log.Fatal(err)
			}
		} else if autoview {
			postdotFd, err = ioutil.TempFile("", "maitre-post")
		}

		if postdotFd != nil {
			defer postdotFd.Close()
			if err := generateDotFile(sources, edges, postdotFd); err != nil {
				log.Error(err)
			}
			postdotFd.Sync()
		}

		if autoview {
			generateAndViewPng(predotFd.Name())
			generateAndViewPng(postdotFd.Name())
		}
	},
}

func init() {
	flags := rootCmd.PersistentFlags()
	flags.StringSliceVar(&sources, "source", nil, "Possible sources")
	flags.StringVar(&vo, "vo", "dteam", "VO")
	flags.Int64Var(&filesize, "size", 1024*1024, "File size")
	flags.StringVar(&dotfile, "dot", "", "Generate dot file")
	flags.StringVar(&predotpath, "pre-dot", "", "Generate dot file without resolving the tree")
	flags.BoolVar(&autoview, "view", false, "Generate png and open them using the browser")

	rootCmd.AddCommand(treeCmd)
}
