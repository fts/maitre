var i = window.location.href.lastIndexOf("/");
var vo = window.location.href.substring(i+1);

function formatDuration(duration) {
    var str = "";
    // We get the duration in nanoseconds!
    duration = duration / 1000000000;

    var hour = Math.floor(duration / 3600);
    var remaining = duration - (hour * 3600);
    if (hour > 0) {
        str += hour + "h "
    }
    var min = Math.floor(remaining / 60);
    remaining = remaining - (min * 60);
    if (min > 0) {
        str += min + "m "
    }
    if (remaining > 0) {
        str += Math.round(remaining*10)/10 + "s"
    }

    if (str == "") {
        str = "0s"
    }

    return str
};

d3.json("/data/" + vo, function(data) {
    var matrix = [], lookup = [], n = data.Nodes.length;
    console.log("Got ", n, " nodes")

    // Prepare matrix
    data.Nodes.forEach(function(node, i) {
        lookup[node.Name] = i;
        node.index = i;
        matrix[i] = d3.range(n).map(function(j) {return null;})
    });

    data.Links.forEach(function(link) {
        var i = lookup[link.Source], j = lookup[link.Target];
        matrix[lookup[link.Source]][lookup[link.Target]] = link.Values;
    });

    // Render
    var table = d3.select("#matrix");

    var firstTr = table.append("tr");
    firstTr.append("td");
    data.Nodes.forEach(function(node, i) {
        firstTr.append("td").attr("class", "storage").text(node.Name);
    });

    data.Nodes.forEach(function(source, i) {
        var row = table.append("tr");
        row.append("td").attr("class", "storage").text(source.Name);
        data.Nodes.forEach(function(dest, j) {
            var v = matrix[lookup[source.Name]][lookup[dest.Name]];
            if (v != null) {
                row.append("td").attr("class", "success-" + Math.floor(v.SuccessRate * 10)).text(
                    Math.round(v.SuccessRate*1000)/10 + "% " +
                    Math.round(v.AvgThroughput/1024/1024) + "MB/s " +
                    formatDuration(v.AvgQueueTime) + " (" +
                    formatDuration(v.EstimatedQueueTime) + ")"
                );
            } else {
                row.append("td").text("");
            }
        });
    });
});
