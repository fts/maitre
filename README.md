Maitre
======

Maitre is a small experimental tool intended to assist users and
experiments on deciding efficient ways of grouping transfers.

On one side, it subscribes to a Stomp endpoint to obtain state transition and terminal
states published by producer FTS. With these messages it populates an in-memory
database (Redis) to keep track of efficiencies and performance of different
links.

On the other side, it takes as arguments a set of storages where a file is present,
its size, and where it has to be replicated. Using the in-memory database, it generates
a graph with all possible paths between the provided storages and their weights.
It then applies the [Edmonds' algorithm](https://en.wikipedia.org/wiki/Edmonds%27_algorithm)
to find the spanning arborescence with the minimum (known) cost.

## Cost heuristic
The cost of an edge (link) is calculated as follows:

```
Estimated Queue Time = Average Queue Time / Success Rate
Estimated Transfer Time = File Size / Average Throughput
Cost = Estimated Queue Time + Estimated Transfer Time
```

The success rate is accounted when calculating the queue time, since is can
be considered the likelihood of having to requeue a transfer to finish successfuly.

* A Success Rate of 50% doubles the Average Queue Time.
* A Success Rate of 0% makes the Average Queue Time infinite, since no matter
how many times the transfer is retried, without changes the file will never go through.

## Examples

```bash
maitre tree --source "gsiftp://eosatlassftp.cern.ch" --source "srm://ccsrm.in2p3.fr" --vo atlas "srm://golias100.farm.particle.cz" "srm://srm.triumf.ca" "srm://f-dpm001.grid.sinica.edu.tw" "srm://t2-dpm-01.na.infn.it" --view
```

### Initial graph from internal data
![Before](img/before.png)

### Spanning tree
![After](img/after.png)
