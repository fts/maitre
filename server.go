/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/dustin/gojson"
	"github.com/spf13/cobra"
	"gitlab.cern.ch/fts/maitre/maitre"
	"net/http"
	"path/filepath"
	"strings"
)

var (
	bind string
)

type (
	node struct {
		Name  string
		Group int
	}

	link struct {
		Source string
		Target string
		Values maitre.LinkInfo
	}

	data struct {
		Nodes  []*node
		Links  []*link
		Groups []string `json:"-"`
	}
)

func (d *data) GetGroupID(domain string) int {
	for i, d := range d.Groups {
		if d == domain {
			return i + 1
		}
	}
	d.Groups = append(d.Groups, domain)
	return len(d.Groups)
}

func (d *data) AppendNode(name string) {
	for _, node := range d.Nodes {
		if node.Name == name {
			return
		}
	}

	parts := strings.Split(name, ".")
	domain := parts[len(parts)-1]
	d.Nodes = append(d.Nodes, &node{
		Name:  name,
		Group: d.GetGroupID(domain),
	})
}

func (d *data) AppendLink(source, target string, values maitre.LinkInfo) {
	d.Links = append(d.Links, &link{
		Source: source,
		Target: target,
		Values: values,
	})
}

func writeError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(err.Error()))
}

func handleDataRequest(w http.ResponseWriter, r *http.Request) {
	l := log.WithField("url", r.URL)
	dir, vo := filepath.Split(r.URL.Path)
	if dir != "/data/" || vo == "" {
		l.Warn("Not found")
		http.NotFound(w, r)
		return
	}

	links, err := instance.GetLinks()
	if err != nil {
		l.WithError(err).Error("Failed to get the links")
		writeError(w, err)
		return
	}

	data := data{}
	for _, link := range links {
		if link.VoName == vo {
			data.AppendNode(link.Source)
			data.AppendNode(link.Destination)

			info, err := instance.GetLinkInfo(link)
			if err != nil {
				log.Error(err)
				continue
			}
			data.AppendLink(link.Source, link.Destination, info)
		}
	}

	raw, err := json.Marshal(data)
	if err != nil {
		l.WithError(err).Error("Failed to serialize")
		writeError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(raw)
	l.Info("OK")
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	l := log.WithField("url", r.URL)
	_, vo := filepath.Split(r.URL.Path)
	if vo == "" {
		l.Info("Redirect")
		http.Redirect(w, r, "/dteam", http.StatusFound)
		return
	}
	http.ServeFile(w, r, "static/index.html")
}

var serverCmd = &cobra.Command{
	Use: "server",
	Run: func(cmd *cobra.Command, args []string) {
		http.Handle("/favicon.ico", http.NotFoundHandler())
		http.HandleFunc("/index.js", func(w http.ResponseWriter, r *http.Request) {
			http.ServeFile(w, r, "static/index.js")
		})

		http.HandleFunc("/", handleRoot)
		http.HandleFunc("/data/", handleDataRequest)

		log.Info("Listening on ", bind)

		log.Fatal(http.ListenAndServe(bind, nil))
	},
}

func init() {
	flags := serverCmd.Flags()
	flags.StringVar(&bind, "bind", "localhost:8080", "Bind")
	rootCmd.AddCommand(serverCmd)
}
