/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package maitre

import (
	log "github.com/Sirupsen/logrus"
	"github.com/ayllon/go/algo"
	"time"
)

type (
	// TransferEdge models an edge on a transfer tree
	TransferEdge struct {
		Link
		EstimatedTime time.Duration
	}

	// TaskDescription defines the transfer target that must be optimized
	TaskDescription struct {
		// The filesize to be transferred, in bytes
		FileSize int64
		// The vo name that is going to do the transfer
		VoName string
		// Sources are the known storages that already have the file
		Sources []string
		// Destinations are the storages where the file must be replicated
		Destinations []string
	}
)

// GetSource returns the source storage
func (e TransferEdge) GetSource() string {
	return e.Source
}

// GetDestination returns the destination storage
func (e TransferEdge) GetDestination() string {
	return e.Destination
}

// GetWeight returns the link weight, measured as total estimated time until completion
func (e TransferEdge) GetWeight() float64 {
	return e.EstimatedTime.Seconds()
}

// SpanningTree returns the optimal transfer tree for the given task
func (m *Maitre) SpanningTree(task *TaskDescription) ([]algo.Edge, error) {
	edges, err := m.GenerateEdgeList(task)
	if err != nil {
		return nil, err
	}

	var dmst []algo.Edge
	// Empty node is the root (the parent of all sources)
	algo.Edmonds("", edges, &dmst)
	return dmst, nil
}

// GenerateEdgeList returns the edges involved in the given task.
// This is, known connections between sources and destinations.
// All sources are connected to an abstract empty node with a link of cost 0. This empty node is used later as
// the root for the spanning tree.
// You can think of this as a cost 0 of replicating the file to those places where it already is.
func (m *Maitre) GenerateEdgeList(task *TaskDescription) ([]algo.Edge, error) {
	var edges []algo.Edge

	// All storages are potential sources
	// We do not add edges *towards* an storage that already has the file
	potentialSources := make([]string, len(task.Sources), len(task.Sources)+len(task.Destinations))
	copy(potentialSources, task.Sources)
	potentialSources = append(potentialSources, task.Destinations...)

	for _, source := range potentialSources {
		for _, dest := range task.Destinations {
			if source == dest {
				continue
			}
			link := Link{VoName: task.VoName, Source: source, Destination: dest}
			info, err := m.GetLinkInfo(link)
			// If everything is 0, we do not know enough to consider this link
			if info.SuccessRate == 0 && info.AvgThroughput == 0 && info.AvgQueueTime == 0 {
				log.Debug("Not connected: ", link)
			} else if err != nil {
				return nil, err
			} else {
				log.Debug("Found link: ", link, " (", info, ")")
				edges = append(edges, TransferEdge{
					Link:          link,
					EstimatedTime: info.GetEstimatedTotalTime(task.FileSize),
				})
			}
		}
	}

	// Insert infinitely efficient link to storages that already have the file
	for _, source := range task.Sources {
		edges = append(edges, TransferEdge{
			Link:          Link{Source: "", Destination: source, VoName: task.VoName},
			EstimatedTime: 0,
		})
	}

	return edges, nil
}
