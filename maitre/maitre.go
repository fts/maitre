/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package maitre

import (
	"github.com/garyburd/redigo/redis"
	"time"
)

type (
	// Parameters groups the Redis connection parameters, and the lifetime of the stored messages
	Parameters struct {
		Address  string
		Password string
		// Limit of messages to remember
		Limit int
		// TTL for the keys, in seconds
		TTL int
	}

	// Maitre wraps the connection and internal state of the system
	Maitre struct {
		params Parameters
		pool   redis.Pool
	}
)

// New creates a new Maitre instance
func New(params Parameters) (*Maitre, error) {
	if params.Limit == 0 {
		params.Limit = 50
	}
	if params.TTL == 0 {
		params.TTL = 3600
	}
	return &Maitre{
		params: params,
		pool: redis.Pool{
			Dial: func() (redis.Conn, error) {
				return redis.Dial("tcp", params.Address, redis.DialPassword(params.Password))
			},
			MaxIdle:     10,
			MaxActive:   100,
			IdleTimeout: 30 * time.Second,
			Wait:        true,
		},
	}, nil
}

// Close release the resources
func (m *Maitre) Close() {
	m.pool.Close()
}
