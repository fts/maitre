/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package maitre

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"math"
	"strings"
	"time"
)

// Link models a link between two storages, for a given VO
type Link struct {
	Source, Destination string
	VoName              string
}

// LinkInfo models the statistics for a given link
type LinkInfo struct {
	SuccessRate   float32
	AvgQueueTime  time.Duration
	AvgThroughput int64
}

// GetLinks returns a slice with all the stored links
func (m *Maitre) GetLinks() ([]Link, error) {
	db := m.pool.Get()
	if db.Err() != nil {
		return nil, db.Err()
	}
	defer db.Close()

	keys, err := redis.Strings(db.Do("SMEMBERS", knownLinks))
	if err != nil {
		return nil, err
	}

	links := make([]Link, 0, len(keys))
	for _, key := range keys {
		parts := strings.Split(key, keySeparator)
		link := Link{
			Source:      parts[0],
			Destination: parts[1],
			VoName:      parts[2],
		}
		links = append(links, link)
	}

	return links, nil
}

// GetLinkInfo returns the link stats bound to the given link
func (m *Maitre) GetLinkInfo(link Link) (info LinkInfo, err error) {
	db := m.pool.Get()
	if db.Err() != nil {
		err = db.Err()
		return
	}
	defer db.Close()

	if info.AvgQueueTime, err = getAvgQueueTime(db, link); err != nil {
		return
	}
	if info.SuccessRate, err = getSuccessRate(db, link); err != nil {
		return
	}
	if info.AvgThroughput, err = getAvgThroughput(db, link); err != nil {
		return
	}
	return
}

// getAvgQueueTime returns the average time between a SUBMITTED and an ACTIVE message (time in queue)
func getAvgQueueTime(db redis.Conn, link Link) (time.Duration, error) {
	queueKey := buildKey(queueKeyPrefix, link.Source, link.Destination, link.VoName)
	reply, err := redis.Strings(db.Do("LRANGE", queueKey, 0, -1))
	if err != nil {
		return 0 * time.Second, err
	}
	if len(reply) == 0 {
		return 0 * time.Second, nil
	}

	total := time.Duration(0)
	for _, str := range reply {
		if value, err := time.ParseDuration(str); err == nil {
			if value.Nanoseconds() < 0 {
				logrus.Warn("Invalid duration value!")
			}
			total += value
		}
	}

	duration := time.Duration(total.Nanoseconds() / int64(len(reply)))
	return duration, nil
}

// getCount returns how many elements there are for a given link and prefix
func getCount(db redis.Conn, link Link, prefix string) (int, error) {
	key := buildKey(prefix, link.Source, link.Destination, link.VoName)
	val, err := redis.Int(db.Do("LLEN", key))
	if err == redis.ErrNil {
		return 0, nil
	}
	return val, err
}

// getSuccessRate returns the ratio between successes and total transfers (success/total), as a value
// between 0 and 1.
// If the total is 0, the rate is considered 0
func getSuccessRate(db redis.Conn, link Link) (float32, error) {
	failedCount, err := getCount(db, link, failedKeyPrefix)
	if err != nil {
		return 0, err
	}
	finishedCount, err := getCount(db, link, successKeyPrefix)
	if err != nil {
		return 0, err
	}
	if failedCount+finishedCount > 0 {
		return float32(finishedCount) / float32(failedCount+finishedCount), nil
	}
	return 0.0, nil
}

// getAvgThroughput calculates the average throughput *per file*
func getAvgThroughput(db redis.Conn, link Link) (int64, error) {
	finishedKey := buildKey(successKeyPrefix, link.Source, link.Destination, link.VoName)
	reply, err := redis.Values(db.Do("LRANGE", finishedKey, 0, -1))
	if err != nil {
		return 0, err
	}
	if len(reply) == 0 {
		return 0, nil
	}

	var throughputs []int64
	err = redis.ScanSlice(reply, &throughputs)
	if err != nil {
		return 0, err
	}

	var total int64
	for _, thr := range throughputs {
		total += thr
	}

	return total / int64(len(throughputs)), nil
}

// GetEstimatedQueueTime returns the estimated time a transfer will spend on the queue for the link.
// Low success rate penalize the queue time.
func (link *LinkInfo) GetEstimatedQueueTime() time.Duration {
	var estimatedQueueTime time.Duration
	if link.SuccessRate > 0 {
		estimatedQueueTime = time.Duration(float32(link.AvgQueueTime.Nanoseconds()) / link.SuccessRate)
	} else {
		estimatedQueueTime = time.Duration(math.MaxInt64)
	}
	return estimatedQueueTime
}

// GetEstimatedTotalTime returns the estimated time a transfer will take until it finished.
// It is calculated as the estimated queue time + estimated transfer time (filesize / avg throughput)
func (link *LinkInfo) GetEstimatedTotalTime(fsize int64) time.Duration {
	if link.AvgThroughput > 0 {
		return link.GetEstimatedQueueTime() + time.Duration((fsize/link.AvgThroughput)*int64(time.Second))
	}
	return link.GetEstimatedQueueTime()
}
