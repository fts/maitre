/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package maitre

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"gitlab.cern.ch/fts/maitre/maitre/messages"
	"time"
)

func addLink(db redis.Conn, source, dest, vo string) error {
	_, err := db.Do("SADD", knownLinks, buildKey(source, dest, vo))
	return err
}

func (m *Maitre) feedSubmitted(state *messages.StateChange) error {
	db := m.pool.Get()
	if db.Err() != nil {
		return db.Err()
	}
	defer db.Close()

	if err := addLink(db, state.SourceSe, state.DestSe, state.VoName); err != nil {
		return err
	}

	submitTimeKey := buildKey(submittedKeyPrefix, state.JobID, fmt.Sprint(state.FileID))
	if _, err := db.Do("SET", submitTimeKey, state.Timestamp.String()); err != nil {
		return fmt.Errorf("Failed to set submit time: %s", err)
	}
	if _, err := db.Do("EXPIRE", submitTimeKey, m.params.TTL); err != nil {
		return fmt.Errorf("Failed to set expiration time: %s", err)
	}

	log.Debugf("[%s] %s SUBMITTED", state.Timestamp, submitTimeKey)
	return nil
}

func getSubmitTime(db redis.Conn, key string) (submitTime time.Time, err error) {
	var submitTimeStr string
	submitTimeStr, err = redis.String(db.Do("GET", key))
	if err != nil {
		return
	}
	if _, err = db.Do("DEL", key); err != nil {
		err = fmt.Errorf("Failed to delete submit time: %s", err)
		return
	}
	submitTime, err = time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", submitTimeStr)
	return
}

func (m *Maitre) feedActive(state *messages.StateChange) error {
	db := m.pool.Get()
	if db.Err() != nil {
		return db.Err()
	}
	defer db.Close()

	if err := addLink(db, state.SourceSe, state.DestSe, state.VoName); err != nil {
		return err
	}

	submitTimeKey := buildKey(submittedKeyPrefix, state.JobID, fmt.Sprint(state.FileID))
	submitTime, err := getSubmitTime(db, submitTimeKey)
	if err == redis.ErrNil {
		log.Debugf("[%s] %s ACTIVE without previous SUBMITTED, skip", state.Timestamp, submitTimeKey)
		return nil
	} else if err != nil {
		return err
	}

	timeInQueue := state.Timestamp.Sub(submitTime)
	if timeInQueue.Nanoseconds() < 0 {
		return fmt.Errorf("Negative queue duration! %s - %s", state.Timestamp, submitTime)
	}

	queueKey := buildKey(queueKeyPrefix, state.SourceSe, state.DestSe, state.VoName)
	if _, err := db.Do("LPUSH", queueKey, timeInQueue); err != nil {
		return fmt.Errorf("Failed to store new duration: %s", err)
	}
	// Purge old records
	if _, err := db.Do("LTRIM", queueKey, 0, m.params.Limit-1); err != nil {
		return fmt.Errorf("Failed to trim: %s", err)
	}

	log.Infof("[%s] %s ACTIVE (was %s in the queue)", state.Timestamp, submitTimeKey, timeInQueue)
	return nil
}

// FeedStateChange receives a state change and updates the in-memory database with the new data
func (m *Maitre) FeedStateChange(state *messages.StateChange) error {
	// We only care about queue time
	switch state.FileState {
	case "SUBMITTED":
		return m.feedSubmitted(state)
	case "ACTIVE":
		return m.feedActive(state)
	}
	return nil
}

// FeedTransferTerminal receives a terminal message and updates the in-memory database with the new data
func (m *Maitre) FeedTransferTerminal(terminal *messages.Terminal) error {
	// Non interested on canceled transfers
	if terminal.FinalTransferState == "Abort" {
		return nil
	}

	db := m.pool.Get()
	if db.Err() != nil {
		return db.Err()
	}
	defer db.Close()

	if err := addLink(db, terminal.SourceSe, terminal.DestSe, terminal.VoName); err != nil {
		return err
	}

	var fileThroughput uint64
	if terminal.FinalTransferState == "Ok" && terminal.TransferStart != nil && terminal.TransferEnd != nil {
		fileThroughput = uint64(float64(terminal.BytesTransferred) / terminal.TransferEnd.Sub(*terminal.TransferStart).Seconds())
	}

	prefix := successKeyPrefix
	if terminal.FinalTransferState == "Error" {
		prefix = failedKeyPrefix
	}

	terminalKey := buildKey(prefix, terminal.SourceSe, terminal.DestSe, terminal.VoName)

	if _, err := db.Do("LPUSH", terminalKey, fileThroughput); err != nil {
		return fmt.Errorf("Failed to store new terminal: %s", err)
	}
	if _, err := db.Do("EXPIRE", terminalKey, m.params.TTL); err != nil {
		return fmt.Errorf("Failed to set expiration time: %s", err)
	}

	// Purge old records
	if _, err := db.Do("LTRIM", terminalKey, 0, m.params.Limit-1); err != nil {
		return fmt.Errorf("Failed to trim: %s", err)
	}

	log.Infof("%s: %s++", terminalKey, terminal.FinalTransferState)

	return nil
}
