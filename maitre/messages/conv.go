/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package messages

import (
	"fmt"
	"math"
	"net"
	"net/url"
	"strconv"
	"time"
)

func str2bool(s string) bool {
	return s == "true"
}

func str2uint64(s string) uint64 {
	v, _ := strconv.ParseUint(s, 10, 64)
	return v
}

func str2time(s string) *time.Time {
	if s == "" {
		return nil
	}

	v, _ := strconv.ParseFloat(s, 64)
	seconds := int64(math.Ceil(v / 1000))
	t := time.Unix(seconds, 0)
	return &t
}

func str2int(s string) int {
	v, _ := strconv.ParseInt(s, 10, 32)
	return int(v)
}

func seFromUrl(raw string) string {
	parsed, err := url.Parse(raw)
	if err != nil {
		return raw
	}
	host, _, err := net.SplitHostPort(parsed.Host)
	if err != nil {
		host = parsed.Host
	}
	return fmt.Sprint(parsed.Scheme, "://", host)
}
