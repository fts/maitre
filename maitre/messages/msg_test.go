/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package messages

import (
	"encoding/json"
	"reflect"
	"testing"
)

func validate(t *testing.T, value, expected interface{}, msg string) {
	if !reflect.DeepEqual(value, expected) {
		t.Error(msg, " ", value, " != ", expected)
	}
}

func TestDeserializeState(t *testing.T) {
	// Extracted from a real FTS
	stateChangeMsg := []byte(`{
	"endpnt" : "fts3-devel.cern.ch",
	"user_dn" : "",
	"src_url" : "mock://test.cern.ch/wmwc/azbd/qaol?size=1048576",
	"dst_url" : "mock://test.cern.ch/enyp/xhvi/mfwn?size_post=1048576&time=1",
	"vo_name" : "dteam",
	"source_se" : "mock://test.cern.ch",
	"dest_se" : "mock://test.cern.ch",
	"job_id" : "10c2b352-6a75-11e6-be45-02163e00ed28",
	"file_id" : 3320053,
	"job_state" : "FINISHEDDIRTY",
	"file_state" : "FINISHED",
	"retry_counter" : 0,
	"retry_max" : -1,
	"timestamp" : 1472096176497,
	"job_metadata" : {
		"test" : "test_multiple_one_exists",
		"label" : "fts3-tests"
	},
	"file_metadata" : {
		"test" : "test_multiple_one_exists",
		"label" : "fts3-tests"
	}
}`)

	state := StateChange{}
	if err := json.Unmarshal(stateChangeMsg, &state); err != nil {
		t.Fatal(err)
	}

	validate(t, state.Endpoint, "fts3-devel.cern.ch", "Unexpected endpoint")
	validate(t, state.JobID, "10c2b352-6a75-11e6-be45-02163e00ed28", "Unexpected job id")
	validate(t, state.FileID, int64(3320053), "Unexpected file id")
	validate(t, state.SourceUrl, "mock://test.cern.ch/wmwc/azbd/qaol?size=1048576", "Unexpected source")
	validate(t, state.DestUrl, "mock://test.cern.ch/enyp/xhvi/mfwn?size_post=1048576&time=1", "Unexpected destination")
	validate(t, state.VoName, "dteam", "Unexpected vo")
	validate(t, state.SourceSe, "mock://test.cern.ch", "Unexpected source storage")
	validate(t, state.DestSe, "mock://test.cern.ch", "Unexpected destination storage")
	validate(t, state.JobState, "FINISHEDDIRTY", "Unexpected job state")
	validate(t, state.FileState, "FINISHED", "Unexpected file state")
}

func TestDeserializeTerminal(t *testing.T) {
	// Extracted from a real FTS
	terminalMsg := []byte(`{
        "tr_id" : "2016-08-25-0336__test2.cern.ch__aplace.es__3320054__16f49bd2-6a75-11e6-9899-02163e0170e3",
        "endpnt" : "fts3-devel.cern.ch",
        "src_srm_v" : "",
        "dest_srm_v" : "",
        "vo" : "dteam",
        "src_url" : "mock://test2.cern.ch/mzab/wnug/ouqw?errno=2",
        "dst_url" : "mock://aplace.es/hwrx/zpkd/mpvt?size_post=1048576&time=1",
        "src_hostname" : "mock://test2.cern.ch",
        "dst_hostname" : "mock://aplace.es",
        "src_site_name" : "",
        "dst_site_name" : "",
        "t_channel" : "test2.cern.ch__aplace.es",
        "timestamp_tr_st" : 0,
        "timestamp_tr_comp" : 0,
        "timestamp_chk_src_st" : 0,
        "timestamp_chk_src_ended" : 0,
        "timestamp_checksum_dest_st" : 0,
        "timestamp_checksum_dest_ended" : 0,
        "t_timeout" : 3600,
        "chk_timeout" : 0,
        "t_error_code" : 2,
        "tr_error_scope" : "SOURCE",
        "t_failure_phase" : "TRANSFER_PREPARATION",
        "tr_error_category" : "NO_SUCH_FILE_OR_DIRECTORY",
        "t_final_transfer_state" : "Error",
        "tr_bt_transfered" : 0,
        "nstreams" : 0,
        "buf_size" : 0,
        "tcp_buf_size" : 0,
        "block_size" : 0,
        "f_size" : 0,
        "time_srm_prep_st" : 0,
        "time_srm_prep_end" : 0,
        "time_srm_fin_st" : 0,
        "time_srm_fin_end" : 0,
        "srm_space_token_src" : "",
        "srm_space_token_dst" : "",
        "t__error_message" : "No such file or directory",
        "tr_timestamp_start" : 1472096183054,
        "tr_timestamp_complete" : 1472096183055,
        "channel_type" : "urlcopy",
        "user_dn" : "/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ftssuite/CN=737188/CN=Robot: fts3 testsuite",
        "file_metadata" : null,
        "job_metadata" : {
                "test" : "test_multiple_sources_all_enoent",
                "label" : "fts3-tests"
        },
        "retry" : 0,
        "retry_max" : 0,
        "job_m_replica" : false,
        "job_state" : "ACTIVE",
        "is_recoverable" : false,
        "ipv6" : false
}
`)

	terminal := Terminal{}
	if err := json.Unmarshal(terminalMsg, &terminal); err != nil {
		t.Fatal(err)
	}

	validate(t, terminal.JobID, "16f49bd2-6a75-11e6-9899-02163e0170e3", "Unexpected job id")
	validate(t, terminal.FileID, int64(3320054), "Unexpected file id")
	validate(t, terminal.Endpoint, "fts3-devel.cern.ch", "Unexpected endpoint")
	validate(t, terminal.VoName, "dteam", "Unexpected vo")
	validate(t, terminal.SourceSe, "mock://test2.cern.ch", "Unexpected source storage")
	validate(t, terminal.DestSe, "mock://aplace.es", "Unexpected destination storage")
	validate(t, terminal.FinalTransferState, "Error", "Unexpected final transfer state")
	validate(t, terminal.ErrorCode, 2, "Unexpected error code")
	validate(t, terminal.IsRecoverable, false, "Unexpected recoverable mark")
}

func TestStorage(t *testing.T) {
	se := seFromUrl("gsiftp://eoscmsftp.cern.ch//eos/cms/store/PhE")
	if se != "gsiftp://eoscmsftp.cern.ch" {
		t.Error("Failed, got ", se)
	}
}
