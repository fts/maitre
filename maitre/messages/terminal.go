/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package messages

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// terminalRaw is the message as sent by FTS
type terminalRaw struct {
	Endpoint            string `json:"endpnt"`

	TransferID          string `json:"tr_id"`

	FinalTransferState  string `json:"t_final_transfer_state"`
	JobState            string `json:"job_state"`

	UserDn              string `json:"user_dn"`
	VoName              string `json:"vo"`

	FileSize            string `json:"f_size"`
	BytesTransferred    string `json:"tr_bt_transfered"`
	SourceUrl           string `json:"src_url"`
	DestUrl             string `json:"dst_url"`
	SourceHost          string `json:"src_hostname"`
	DestHost            string `json:"dst_hostname"`

	TransferStart       string `json:"timestamp_tr_st"`
	TransferEnd         string `json:"timestamp_tr_comp"`

	SourceChecksumStart string `json:"timestamp_chk_src_st"`
	SourceChecksumEnd   string `json:"timestamp_chk_src_ended"`
	DestChecksumStart   string `json:"timestamp_checksum_dest_st"`
	DestChecksumEnd     string `json:"timestamp_checksum_dest_ended"`

	Timeout             string `json:"t_timeout"`
	NoStreams           string `json:"nstreams"`
	BufferSize          string `json:"buf_size"`
	IPv6       string `json:"ipv6"`

	ErrorMessage  string `json:"t__error_message"`
	ErrorCode     string `json:"t_error_code"`
	ErrorScope    string `json:"tr_error_scope"`
	FailurePhase  string `json:"t_failure_phase"`
	ErrorCategory string `json:"tr_error_category"`
	IsRecoverable string `json:"is_recoverable"`

	IsMultiReplica string `json:"job_m_replica"`

	JobMetadata  json.RawMessage `json:"job_metadata"`
	FileMetadata json.RawMessage `json:"file_metadata"`
}

// Terminal extends TerminalRaw with the JobID and FileID extracted from the TransferID
type Terminal struct {
	Endpoint string

	TransferID string
	JobID      string
	FileID     uint64

	FinalTransferState string
	JobState           string
	IsMultiReplica     bool

	UserDn string
	VoName string

	FileSize         uint64
	BytesTransferred uint64
	SourceUrl        string
	DestUrl          string
	SourceSe         string
	DestSe           string

	TransferStart *time.Time
	TransferEnd   *time.Time

	SourceChecksumStart *time.Time
	SourceChecksumEnd   *time.Time
	DestChecksumStart   *time.Time
	DestChecksumEnd     *time.Time

	Timeout    uint64
	NoStreams  int
	BufferSize int
	IPv6       bool

	ErrorMessage  string
	ErrorCode     int
	ErrorScope    string
	FailurePhase  string
	ErrorCategory string
	IsRecoverable bool

	JobMetadata  json.RawMessage
	FileMetadata json.RawMessage
}

// String return a printable representation
func (t *Terminal) String() string {
	return fmt.Sprintf("%s %d %s", t.JobID, t.FileID, t.FinalTransferState)
}

// UnmarshalJSON does a custom decoding, since JobID and FileID need to be extracted
// for TransferId
func (t *Terminal) UnmarshalJSON(data []byte) error {
	var err error
	var raw terminalRaw

	if err = json.Unmarshal(data, &raw); err != nil {
		return err
	}
	parts := strings.Split(raw.TransferID, "__")
	jobID := parts[4]
	fileID, err := strconv.ParseUint(parts[3], 10, 64)
	if err != nil {
		return err
	}

	*t = Terminal{
		Endpoint: raw.Endpoint,

		TransferID: raw.TransferID,
		JobID:      jobID,
		FileID:     fileID,

		FinalTransferState: raw.FinalTransferState,
		JobState:           raw.JobState,
		IsMultiReplica:     str2bool(raw.IsMultiReplica),

		UserDn: raw.UserDn,
		VoName: raw.VoName,

		FileSize:         str2uint64(raw.FileSize),
		BytesTransferred: str2uint64(raw.BytesTransferred),
		SourceUrl:        raw.SourceUrl,
		DestUrl:          raw.DestUrl,
		SourceSe:         seFromUrl(raw.SourceUrl),
		DestSe:           seFromUrl(raw.DestUrl),

		TransferStart: str2time(raw.TransferStart),
		TransferEnd:   str2time(raw.TransferEnd),

		SourceChecksumStart: str2time(raw.SourceChecksumStart),
		SourceChecksumEnd:   str2time(raw.SourceChecksumEnd),
		DestChecksumStart:   str2time(raw.DestChecksumStart),
		DestChecksumEnd:     str2time(raw.DestChecksumEnd),

		Timeout:    str2uint64(raw.Timeout),
		NoStreams:  str2int(raw.NoStreams),
		BufferSize: str2int(raw.BufferSize),
		IPv6:       str2bool(raw.IPv6),

		ErrorMessage:  raw.ErrorMessage,
		ErrorCode:     str2int(raw.ErrorCode),
		ErrorScope:    raw.ErrorScope,
		FailurePhase:  raw.FailurePhase,
		ErrorCategory: raw.ErrorCategory,
		IsRecoverable: str2bool(raw.IsRecoverable),

		JobMetadata:  raw.JobMetadata,
		FileMetadata: raw.FileMetadata,
	}
	return nil
}
