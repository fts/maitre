/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package messages

import (
	"encoding/json"
	"fmt"
	"time"
)

type stateRaw struct {
	Timestamp    string          `json:"timestamp"`
	Endpoint     string          `json:"endpnt"`
	JobID        string          `json:"job_id"`
	FileID       string          `json:"file_id"`
	JobState     string          `json:"job_state"`
	FileState    string          `json:"file_state"`
	SourceUrl    string          `json:"src_url"`
	DestUrl      string          `json:"dst_url"`
	SourceSe     string          `json:"source_se"`
	DestSe       string          `json:"dest_se"`
	UserDn       string          `json:"user_dn"`
	VoName       string          `json:"vo_name"`
	RetryCounter string          `json:"retry_counter"`
	RetryMax     string          `json:"retry_max"`
	JobMetadata  json.RawMessage `json:"job_metadata"`
	FileMetadata json.RawMessage `json:"file_metadata"`
}

// StateChange is the message FTS sends when a transfers changes state
type StateChange struct {
	Timestamp    time.Time
	Endpoint     string
	JobID        string
	FileID       uint64
	JobState     string
	FileState    string
	SourceUrl    string
	DestUrl      string
	SourceSe     string
	DestSe       string
	UserDn       string
	VoName       string
	RetryCounter int
	RetryMax     int
	JobMetadata  json.RawMessage
	FileMetadata json.RawMessage
}

// String generates a printable representation of the message
func (sc *StateChange) String() string {
	return fmt.Sprintf("[%d] %s %d => %s %s", sc.Timestamp, sc.JobID, sc.FileID, sc.JobState, sc.FileState)
}

// UnmarshalJSON does a custom decoding
func (sc *StateChange) UnmarshalJSON(data []byte) error {
	var err error
	var raw stateRaw
	if err = json.Unmarshal(data, &raw); err != nil {
		return err
	}
	*sc = StateChange{
		Timestamp:    *str2time(raw.Timestamp),
		Endpoint:     raw.Endpoint,
		JobID:        raw.JobID,
		FileID:       str2uint64(raw.FileID),
		JobState:     raw.JobState,
		FileState:    raw.FileState,
		SourceUrl:    raw.SourceUrl,
		DestUrl:      raw.DestUrl,
		SourceSe:     raw.SourceSe,
		DestSe:       raw.DestSe,
		UserDn:       raw.UserDn,
		VoName:       raw.VoName,
		RetryCounter: str2int(raw.RetryCounter),
		RetryMax:     str2int(raw.RetryMax),
		JobMetadata:  raw.JobMetadata,
		FileMetadata: raw.FileMetadata,
	}
	return nil
}
